from app import app, manager

@manager.command
def runserver():
    app.run()

if __name__ == "__main__":
    manager.run()
