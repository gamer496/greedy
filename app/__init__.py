from flask import Flask
from flask_script import Manager
from flask_cors import CORS
import oauth2
import api_keys

app = Flask(__name__)
manager = Manager(app)
CORS(app)
app.config.from_object("config")

#setup of twitter client
def setup_twitter_client():
    consumer = oauth2.Consumer(key = api_keys.CONSUMER_TOKEN, secret =
                               api_keys.CONSUMER_SECRET)
    token = oauth2.Token(key = api_keys.ACCESS_TOKEN, secret =
                         api_keys.ACCESS_SECRET)
    return oauth2.Client(consumer, token)

twitter_client = setup_twitter_client()

from app import views
