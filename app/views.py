from flask import jsonify
from app import app, twitter_client
import api_keys
import requests
from HTMLParser import HTMLParser
import duckduckgo
import json
from multiprocessing import Process, Queue
import time

h = HTMLParser()

RESULT_ERROR = "something went wrong. Could not properly fetch results"
GOOGLE_RESULTS = "google_results"
DUCKDUCKGO_RESULTS = "duckduckgo_results"
TWITTER_RESULTS = "twitter_results"

def sanitize_to_human(s):
    return h.unescape(s.encode("utf8", "ignore"))


def get_google_results(query, que):
    try:
        google_url = api_keys.get_google_url(query)
        re = requests.get(google_url)
        re = re.json()
        query_results = re[api_keys.GOOGLE_CONTAINER_OBJECT]
        results = []
        for result in query_results:
            results.append({"url": result[api_keys.GOOGLE_URL_KEY], "description": sanitize_to_human(result[api_keys.GOOGLE_DESCRIPTION_KEY])})
        que.put({GOOGLE_RESULTS: results})
    except:
        que.put({GOOGLE_RESULTS: RESULT_ERROR})

def get_duckduckgo_results(query, que):
    try:
        query_results = duckduckgo.query(query)
        results = []
        if query_results.type == "answer":
            all_results = query_results.results[:5]
            for result in all_results:
                results.append({"url": result.url, "description": result.text})
        elif query_results.type == "disambiguation":
            all_results = query_results.related[:5]
            for result in all_results:
                results.append({"url": result.url, "description": result.text})
        que.put({DUCKDUCKGO_RESULTS: results})
    except:
        que.put({DUCKDUCKGO_RESULTS: RESULT_ERROR})

def get_twitter_results(query, que):
    try:
        resp, content = twitter_client.request("https://api.twitter.com/1.1/search/tweets.json?q=%s&count=%d"%(query, api_keys.TWITTER_COUNT))
        data = json.loads(content)["statuses"][:5]
        results = []
        base_url = "https://twitter.com/statuses/"
        for result in data:
            results.append({"url": base_url + str(result["id_str"]), "description": result["text"]})
        que.put({TWITTER_RESULTS: results})
    except:
        que.put({TWITTER_RESULTS: RESULT_ERROR})


@app.route("/results/<q>", methods = ["GET"])
def results(q):
    start = time.time()
    que = Queue()
    p_google = Process(target = get_google_results, args = (q, que))
    p_duckduckgo = Process(target = get_duckduckgo_results, args = (q, que))
    p_twitter = Process(target = get_twitter_results, args = (q, que))
    p_google.start()
    p_twitter.start()
    p_duckduckgo.start()
    p_google.join(1.5)
    p_twitter.join(1)
    p_duckduckgo.join(1)
    if p_google.is_alive():
        p_google.terminate()
        p_google.join()
    if p_duckduckgo.is_alive():
        p_duckduckgo.terminate()
        p_duckduckgo.join()
    if p_twitter.is_alive():
        p_twitter.terminate()
        p_twitter.join()
    d = {}
    while not que.empty():
        d.update(que.get())
    if not TWITTER_RESULTS in d:
        d.update({TWITTER_RESULTS: RESULT_ERROR})
    if not DUCKDUCKGO_RESULTS in d:
        d.update({DUCKDUCKGO_RESULTS: RESULT_ERROR})
    if not GOOGLE_RESULTS in d:
        d.update({GOOGLE_RESULTS: RESULT_ERROR})
    time_taken = time.time() - start
    d.update({"time_taken": time_taken})
    return jsonify(d)
